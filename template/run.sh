#!/bin/bash

function run() {
  ./build.sh 
  if [ $? != 0 ]; then
    return
  fi

  # Run the app with qemu and remote debugging available on port 1234
  qemu-aarch64 -L /usr/aarch64-linux-gnu -g 1234 ./out/app &

  # Connect via gdb and setup nice layout
  gdb-multiarch -q --nh \
    -ex 'set architecture aarch64' \
    -ex 'set sysroot /usr/aarch64-linux-gnu' \
    -ex 'file ./out/app' \
    -ex 'target remote localhost:1234' \
    -ex 'layout src' \
    -ex 'layout regs' \
    -ex 'focus cmd'

  # gdb doesn't close gracefully if the qemu process ends first, so we have to 
  # run reset for the terminal to remain usable
  if [ $? != 0 ]; then
    reset
  fi
}

run
