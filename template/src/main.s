.data
hello: 
  .ascii "Hello!\n"
len = . - hello
bye:
  .ascii "Bye\n"
byelen = . - bye


.text
.global _start
_start:
  mov x0, 1     // FD == 1 == stdout
  ldr x1, =hello // Data to print
  mov x2, 7     // Length of data
  mov x8, 64    // Code for 'write' syscall
  svc 0         // Trigger syscall

  // Syscall exit with status code 0
  mov x8, 93
  mov x0, 0
  svc 0

