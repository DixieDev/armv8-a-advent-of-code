.include "util.s"

/*
read syscall is 63/0x3F
- fd
- char *buf
- count
- returns numnber of bytes read (0 means eof)
- returns -1 on error

openat syscall is 56/0x38
- dirfd, the dir from which to open relative paths. -100 to use cwd
- char *pathname
- flags, O_RDONLY is 0
- mode,

Hello example using 'write' syscall:
  hello: 
    .ascii "Hello!\n"
  len = . - hello

  mov x0, 1      // fd of 1 is stdout
  ldr x1, =hello // Data to print
  mov x2, 7      // Length of data
  mov x8, 64     // Code for 'write' syscall
  svc 0          // Trigger syscall

mmap syscall is 222/0xDE, but not needed for part 1
- addr, set to NULL to let kernel choose
- length, size in _pages_
- protection level
- flags
- file descriptor (can be null)
- offset
*/

.data
str_input_path:
  .ascii "res/input.txt\0"
str_open_err:
  .ascii "Failed to open the file\n"
str_open_len = . - str_open_err

str_read_err:
  .ascii "Failed to read the file\n"
str_read_len = . - str_read_err

.text
.global _start
_start:
  /*
  x10 - Final result
  x11 - Current line value
  x12 - FD for opened file
  x13 - Last read digit on current line
  */

  mov x10, 0
  mov x11, 0
  mov x12, 0
  mov x13, 0

  // Alloc 1 byte on the stack for reading purposes. The stack has to be 
  // aligned on 16-byte boundaries so we alloc 16 bytes instead of just 1.
  sub sp, sp, 16
  str xzr, [sp, 1] // set to 0

  // Open the input file
  mov x8, 56 // openat syscall code 
  mov x0, -100
  ldr x1, =str_input_path
  mov x2, 0
  mov x3, 0
  svc 0

  // Return value is on x0, make sure it's valid
  subs x0, x0, 0 // Subs is like sub except it sets flags
  b.le .Lopen_err

  // Move file descriptor into x12
  mov x12, x0

// Loop, reading each char in the input file until reaching EOF
.Lread_loop_first_digit:
  mov x0, x12
  bl getch

  // Handle error
  subs x0, x0, 0
  b.lt .Lread_err

  // If eof reached then exit loop
  b.eq .Lread_loop_end

  // Move onto the next iteration if the char wasn't a digit
  mov x0, 0
  ldrb w0, [sp, 1]
  subs x1, x0, 48
  b.lt .Lread_loop_first_digit
  subs x2, x0, 57
  b.gt .Lread_loop_first_digit

  // Otherwise multiply the digit by ten and store it in x11
  mov x2, 10
  mul x11, x1, x2

  // Also record the original value in x13, which will be the default value of 
  // the units if no other candidate is found
  mov x13, x1

.Lread_loop_newline:
  mov x0, x12
  bl getch

  // Handle error
  subs x0, x0, 0
  b.lt .Lread_err

  // If eof reached then exit loop
  b.eq .Lread_loop_end

  // Load the char into x0
  mov x0, 0
  ldrb w0, [sp, 1]
  
  // If it's a newline, add the last read digit in x13 to the x11 value, and 
  // then add that to the running total across all lines.
  subs x2, x0, 10
  b.eq .Lread_loop_line_end

  // Move onto the next iteration if the char wasn't a digit 
  subs x2, x0, 57
  b.gt .Lread_loop_newline
  subs x2, x0, 48
  b.lt .Lread_loop_newline

  // Since it's definitely a number, store its numeric value in x13
  mov x13, x2

  // Otherwise keep reading until we find a newline
  b .Lread_loop_newline

.Lread_loop_line_end:
  add x11, x11, x13
  add x10, x10, x11
  mov x11, 0
  mov x13, 0
  b .Lread_loop_first_digit

.Lread_loop_end:
  add x10, x10, x11

  // Finally print the result
  mov x0, x10
  bl print_num

.Lexit:
  // Syscall exit with status code 0
  mov x8, 93
  mov x0, 0
  svc 0

.Lopen_err:
  mov x8, 64
  mov x0, 1
  ldr x1, =str_open_err
  mov x2, str_open_len
  svc 0
  b .Lexit

.Lread_err:
  mov x8, 64
  mov x0, 1
  ldr x1, =str_read_err
  mov x2, str_read_len
  svc 0
  b .Lexit


