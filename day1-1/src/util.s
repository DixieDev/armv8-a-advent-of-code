// Performs an exponent, returning the value in x0
//   Arg 0 (x0) = Base
//   Arg 1 (x1) = Exponent
//   Returns via x0
//   Modifies x0, x1, x2
upow:
  mov x2, x0
  mov x0, 1
  cmp x1, 0
.Lupow_loop:
  b.eq .Lupow_exit
  mul x0, x0, x2
  subs x1, x1, 1
  b .Lupow_loop
.Lupow_exit:
  ret


// Read a char from a file descriptor into the last byte on the stack.
//   Arg (x0) = File descriptor
//   Returns number of bytes read via x0; 0 for EOF, <0 for error
//   The read char is stored at sp+1
//   Modifies x0, x1, x2, x8
getch:
  mov x8, 63 // `read` syscall code

  // We don't need to set x0 because that's the fd arg

  // Buf is the byte at sp+1
  mov x1, sp
  add x1, x1, 1

  mov x2, 1 // Number of bytes to read
  svc 0 // Invoke syscall
  ret


// Prints an unsigned number
//   Arg (x0) = Number to print
//   No return value
//   Modifies x1, x2, x3, x4, x5, x8, x20, x21
//   x0 is preserved for convenience
print_num:
  // Store x30 in x21, so we return to the correct address later
  mov x21, x30

  // Move x0 to x20; we'll restore this later since it's convenient for this 
  // function to preserve x0's value.
  mov x20, x0

  // Start by figuring out how many digits are in the number, so we can
  // allocate stack space
  //
  // x3 contains the current exponent for our powers of 10. Conveniently this is
  // also the number of digits
  mov x3, 0  

.Lprint_num_count_loop:
  mov x0, 10
  mov x1, x3
  bl upow

  udiv x2, x20, x0
  cmp x2, 0
  b.eq .Lprint_num_count_end
  add x3, x3, 1
  b .Lprint_num_count_loop

.Lprint_num_count_end:
  // Allocate space on the stack for each char, plus a linefeed. This
  // also has have 16-byte alignment so we round it up if necessary.
  //
  // The allocation size will be recorded in x5.
  add x0, x3, 1
  bl round16
  mov x5, x0
  sub sp, sp, x5

  // The cursor to write the next char to on the stack
  mov x4, 1

.Lprint_num_store_loop:
  cbz x3, .Lprint_num_store_end
  sub x3, x3, 1
  mov x0, 10
  mov x1, x3
  bl upow
  
  
  udiv x0, x20, x0

  // Mod10 for the value in x0
  mov x2, 10
  udiv x1, x0, x2
  mul x1, x1, x2
  sub x0, x0, x1

  add x0, x0, 48 // Convert to ascii char
  strb w0, [sp, x4]
  add x4, x4, 1
  b .Lprint_num_store_loop

.Lprint_num_store_end:
  // Store line-feed at end of buf
  mov x0, 10 // line-feed ascii code
  strb w0, [sp, x4]
  add x4, x4, 1

  // Print the number
  mov x8, 64
  mov x0, 1
  add x1, sp, 1
  mov x2, x4
  svc 0

  // Free stack space, restore x0 & x30, return
  add sp, sp, x5
  mov x0, x20
  mov x30, x21
  ret


// Rounds an unsigned number up to a multiple of 16
//   Arg (x0) = Number to round
//   Returns the rounded value on x0
//   Modifies x0, x10, x2
round16:
  mov x1, 16
  udiv x2, x0, x1
  mul x2, x2, x1
  sub x1, x0, x2
  cbz x1, .Lround16_ret
  mov x2, 16
  sub x1, x2, x1
  add x0, x0, x1
.Lround16_ret:
  ret

