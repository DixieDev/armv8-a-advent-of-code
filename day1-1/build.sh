# !/bin/bash

function build() {
  mkdir -p out
  mkdir -p obj

  # Assemble to object file
  aarch64-linux-gnu-as src/main.s -o obj/main.o --gen-debug -I src

  if [ $? != 0 ]; then
    return
  fi

  # Link and output binary
  aarch64-linux-gnu-ld -o out/app obj/main.o
}

build
