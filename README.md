# Advent of Code in ARMv8-A Assembly

As if life wasn't hard enough!


## How To Run

Each part of each day is stored in its own folder, once installing the 
prerequisites you _should_ be able to simply `cd` into the folder and run
`./build.sh` followed either by running `./out/app` or running `./run.sh`, the
latter of which will allow you to follow along in gdb, albeit without any 
comments.


## Prerequisites

You will need the following apt packages installed:

- binutils-aarch64-linux-gnu
- qemu-user
- gdb-multiarch

