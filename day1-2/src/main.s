.include "util.s"

/*
read syscall is 63/0x3F
- fd
- char *buf
- count
- returns numnber of bytes read (0 means eof)
- returns -1 on error

openat syscall is 56/0x38
- dirfd, the dir from which to open relative paths. -100 to use cwd
- char *pathname
- flags, O_RDONLY is 0
- mode,

Hello example using 'write' syscall:
  hello: 
    .ascii "Hello!\n"
  len = . - hello

  mov x0, 1      // fd of 1 is stdout
  ldr x1, =hello // Data to print
  mov x2, 7      // Length of data
  mov x8, 64     // Code for 'write' syscall
  svc 0          // Trigger syscall

mmap syscall is 222/0xDE, but not needed for part 1
- addr, set to NULL to let kernel choose
- length, size in _pages_
- protection level
- flags
- file descriptor (can be null)
- offset
*/

.data
str_input_path:
  .ascii "res/input.txt\0"
str_open_err:
  .ascii "Failed to open the file\n"
str_open_len = . - str_open_err

str_read_err:
  .ascii "Failed to read the file\n"
str_read_len = . - str_read_err

// Strings for each number, followed by their lengths
str_one: .ascii "one"
str_one_len: .8byte . - str_one

str_two: .ascii "two"
str_two_len: .8byte . - str_two

str_three: .ascii "three"
str_three_len: .8byte . - str_three

str_four: .ascii "four"
str_four_len: .8byte . - str_four

str_five: .ascii "five"
str_five_len: .8byte . - str_five

str_six: .ascii "six"
str_six_len: .8byte . - str_six

str_seven: .ascii "seven"
str_seven_len: .8byte . - str_seven

str_eight: .ascii "eight"
str_eight_len: .8byte . - str_eight

str_nine: .ascii "nine"
str_nine_len: .8byte . - str_nine

.text
.global _start
_start:
  /*
    x20   = Flag for first digit read
    sp    = 5 byte ring buffer
    sp+ 8 = File descriptor
    sp+16 = Sum of all numbers
    sp+24 = Current line value
    sp+32 = Value of last digit on line
    sp+40 = Ring buffer cursor (points atthe first element of the buffer)
  */

  // Stack space for all variables. Note this has to be aligned to 16-byte 
  // boundaries.
  sub sp, sp, 48

  // Set allocated stack space to 0
  str xzr, [sp, 0]
  str xzr, [sp, 8]
  str xzr, [sp, 16]
  str xzr, [sp, 24]
  str xzr, [sp, 32]
  str xzr, [sp, 40]
  str xzr, [sp, 48]

  // Open the input file
  mov x8, 56 // openat syscall code 
  mov x0, -100
  ldr x1, =str_input_path
  mov x2, 0
  mov x3, 0
  svc 0

  // Return value is on x0, make sure it's valid
  subs x0, x0, 0 // Subs is like sub except it sets flags
  b.le .Lopen_err

  // Store file descriptor on stack
  str x0, [sp, 8]

  // Fill the ring buffer with 5 bytes from the file
  mov x8, 63
  ldr x0, [sp, 8]
  mov x1, sp
  mov x2, 5
  svc 0

  // Handle possible reading error
  subs x0, x0, 0
  b.lt .Lread_err

  // Loop, reading the first byte of the ring buffer until encountering either
  // an integer or the possible start of its English spelling
  mov x20, 0
  b .Lread_loop_start

.Lread_loop:
  // We do this stuff at the start of each iteration after the first one, to
  // hack around some early decisions I made that don't preserve the ring 
  // buffer's state throughout an iteration

  // Call getch and handle errors
  ldr x0, [sp, 8]
  bl getch
  cmp x0, 0
  b.lt .Lread_err

  // Store the new byte in the ring buffer and advance the cursor
  ldr x2, [sp, 40]
  strb w1, [sp, x2]
  add x0, x2, 1
  mov x1, 5
  bl modulo
  str x0, [sp, 40]

.Lread_loop_start:
  // Read the first byte in the ring buffer, then replace it with a new 
  // character from the file and increment the cursor
  ldr x0, [sp, 40] 
  
  // Store the byte in x10 so it doesn't get spirited away by `bl`s
  mov x10, 0
  ldrb w10, [sp, x0]

  // If eof reached then exit loop
  cbz x10, .Lread_loop_end

  // We have three cases we handle differently:
  // - Newlines end the current iteration and record everything found so far
  // - Digits are converted from ASCII to numeric values
  // - English spellings of numbers are parsed

  // Newline check
  cmp x10, '\n'
  b.eq .Lread_loop_line_end

  // If it's less than '0' then we move onto the next iteration
  cmp x10, '0'
  b.lt .Lread_loop

  // If it's more than '9', it might be a letter of the alphabet still
  cmp x10, '9'
  b.gt .Lmaybe_alphabetical

  // Otherwise we know this is a digit so we convert it to its matching numeric value
  sub x0, x10, '0'

.Lread_loop_record_digit:
  // Record the digit as the last digit encountered on this line, so it will 
  // be used as the default value for this line's units if no other candidates
  // are found.
  str x0, [sp, 32]

  // If the first digit has already been read then continue to the next iteration
  cmp x20, 1
  b.eq .Lread_loop

  // Otherwise, if the number of digits read is 0, then multiply the digit by 
  // ten and store it as the current line value.
  mov x1, 10
  mul x0, x0, x1
  str x0, [sp, 24]

  // Set the flag for the first digit having been read
  mov x20, 1
  b .Lread_loop

.Lmaybe_alphabetical:
  cmp x10, 'z'
  b.gt .Lread_loop

  // Compare the buffer contents to each of the number strings
  mov x11, 9 // Our current cursor in the string loop
.Lread_loop_string_loop:
  cbz x11, .Lread_loop

  ldr x0, =str_nine_len  // Address of length in memory
  ldr x1, [x0, 0]             // Value of length
  mov x2, x11            // Counter for iterating through each number

  // Loop through each number until we're at the one specified by x11
.Lread_loop_string_loop_cursor_loop:
  cmp x2, 9
  b.ge .Lread_loop_string_loop_compare
  
  sub x0, x0, x1
  sub x0, x0, 8
  ldr x1, [x0, 0]
  
  add x2, x2, 1
  b .Lread_loop_string_loop_cursor_loop

  // Check if the string on the stack matches the one in our data section,
  // if not then move onto the next iteration of this loop to check the next
  // possible number string, otherwise record the number's numeric value and
  // branch to the code that will actually record it properly.
.Lread_loop_string_loop_compare:
  sub x3, x0, x1   // x3 becomes the address of the string constant
  mov x4, x1       // x4 becomes the length of the string constant
  mov x0, sp       // x0 becomes the address of the ring buffer
  mov x1, 5        // x1 is the length of the ring buffer
  ldr x2, [sp, 40] // x2 is ring buffer's current cursor position
  bl is_ringbuf_str_eq

  sub x11, x11, 1
  cbz x0, .Lread_loop_string_loop
  add x11, x11, 1
  mov x0, x11
  b .Lread_loop_record_digit

.Lread_loop_line_end:
  ldr x0, [sp, 32]
  ldr x1, [sp, 24]
  ldr x2, [sp, 16]
  add x1, x1, x0
  add x2, x2, x1
  str xzr, [sp, 32]
  str xzr, [sp, 24]
  str x2, [sp, 16]
  mov x20, 0
  b .Lread_loop

.Lread_loop_end:
  ldr x1, [sp, 32]
  ldr x0, [sp, 16]
  add x0, x0, x1
  str x0, [sp, 16]

  // Finally print the result
  bl print_num

.Lexit:
  // Syscall exit with status code 0
  mov x8, 93
  mov x0, 0
  svc 0

.Lopen_err:
  mov x8, 64
  mov x0, 1
  ldr x1, =str_open_err
  mov x2, str_open_len
  svc 0
  b .Lexit

.Lread_err:
  mov x8, 64
  mov x0, 1
  ldr x1, =str_read_err
  mov x2, str_read_len
  svc 0
  b .Lexit


