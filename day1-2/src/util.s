// Performs an exponent, returning the value in x0
//   Arg 0 (x0) = Base
//   Arg 1 (x1) = Exponent
//   Returns via x0
//   Modifies x0, x1, x2
upow:
  mov x2, x0
  mov x0, 1
  cmp x1, 0
.Lupow_loop:
  b.eq .Lupow_exit
  mul x0, x0, x2
  subs x1, x1, 1
  b .Lupow_loop
.Lupow_exit:
  ret


// Read a char from a file descriptor.
//   Arg (x0) = File descriptor
//   Returns number of bytes read via x0; 0 for EOF, <0 for error
//   Returns the read byte itself on x1
//   Modifies x0, x1, x2, x8
getch:
  sub sp, sp, 16

  // We don't need to set x0 because that's the fd arg
  mov x8, 63 // `read` syscall code
  mov x1, sp // Buf is the byte at sp
  mov x2, 1 // Number of bytes to read
  svc 0 // Invoke syscall

  // If we're at EOF, set x1 to 0 and exit
  cbz x0, .Lis_eof

  mov x1, 0
  ldrb w1, [sp, 0]
  add sp, sp, 16
  ret

.Lis_eof:
  mov x1, 0
  add sp, sp, 16
  ret


// Prints an unsigned number
//   Arg (x0) = Number to print
//   No return value
//   Modifies x1, x2, x3, x4, x5, x8, x20, x21
//   x0 is preserved for convenience
print_num:
  // Store x30 in x21, so we return to the correct address later
  mov x21, x30

  // Move x0 to x20; we'll restore this later since it's convenient for this 
  // function to preserve x0's value.
  mov x20, x0

  // Start by figuring out how many digits are in the number, so we can
  // allocate stack space
  //
  // x3 contains the current exponent for our powers of 10. Conveniently this is
  // also the number of digits
  mov x3, 0  

.Lprint_num_count_loop:
  mov x0, 10
  mov x1, x3
  bl upow

  udiv x2, x20, x0
  cmp x2, 0
  b.eq .Lprint_num_count_end
  add x3, x3, 1
  b .Lprint_num_count_loop

.Lprint_num_count_end:
  // Allocate space on the stack for each char, plus a linefeed. This
  // also has have 16-byte alignment so we round it up if necessary.
  //
  // The allocation size will be recorded in x5.
  add x0, x3, 1
  bl round16
  mov x5, x0
  sub sp, sp, x5

  // The cursor to write the next char to on the stack
  mov x4, 1

.Lprint_num_store_loop:
  cbz x3, .Lprint_num_store_end
  sub x3, x3, 1
  mov x0, 10
  mov x1, x3
  bl upow
  
  
  udiv x0, x20, x0

  // Mod10 for the value in x0
  mov x2, 10
  udiv x1, x0, x2
  mul x1, x1, x2
  sub x0, x0, x1

  add x0, x0, 48 // Convert to ascii char
  strb w0, [sp, x4]
  add x4, x4, 1
  b .Lprint_num_store_loop

.Lprint_num_store_end:
  // Store line-feed at end of buf
  mov x0, 10 // line-feed ascii code
  strb w0, [sp, x4]
  add x4, x4, 1

  // Print the number
  mov x8, 64
  mov x0, 1
  add x1, sp, 1
  mov x2, x4
  svc 0

  // Free stack space, restore x0 & x30, return
  add sp, sp, x5
  mov x0, x20
  mov x30, x21
  ret


// Rounds an unsigned number up to a multiple of 16
//   Arg (x0) = Number to round
//   Returns the rounded value on x0
//   Modifies x0, x1, x2
round16:
  mov x1, 16
  udiv x2, x0, x1
  mul x2, x2, x1
  sub x1, x0, x2
  cbz x1, .Lround16_ret
  mov x2, 16
  sub x1, x2, x1
  add x0, x0, x1
.Lround16_ret:
  ret

// Computes a%b of unsigned values.
//   Arg0 (x0) = a
//   Arg1 (x1) = b
//   Returns the result on x0
//   Modifies x0, x2
modulo:
  udiv x2, x0, x1
  mul x2, x2, x1
  sub x0, x0, x2
  ret


// Determines if two strings are equal, 
//   Arg0 (x0) = Address of a string
//   Arg2 (x1) = Address of its expected value
//   Arg3 (x2) = The length of the expected string
//   Returns a boolean via x0: 1 if both strings are identical, otherwise 0
is_str_eq:
  mov x6, 1
  mov x3, 0
  mov x4, 0
  mov x5, 0

.Lstr_eq_loop:
  cmp x3, x2
  b.ge .Lstr_eq_end

  ldrb w4, [x0, x3]
  ldrb w5, [x1, x3]

  cmp x4, x5
  b.ne .Lstr_eq_false

  add x3, x3, 1
  b .Lstr_eq_loop

.Lstr_eq_false:
  mov x6, 0
.Lstr_eq_end:
  mov x0, x6
  ret


// Determines if two strings are equal, when one is stored in a ring buffer
//   Arg0 (x0) = Address of the ring buffer
//   Arg1 (x1) = Length of the ring buffer
//   Arg2 (x2) = Starting index within the ring buffer
//   Arg3 (x3) = Address of another string
//   Arg4 (x4) = The length of the expected string
//   Returns a boolean via x0: 1 if both strings are identical, otherwise 0
is_ringbuf_str_eq:
  /*
    sp+ 0 = Ring buf addr
    sp+ 8 = Ring buf len
    sp+16 = Ring buf start
    sp+24 = String addr
    sp+32 = String len
    sp+40 = x30
  */
  sub sp, sp, 48
  str x0, [sp, 0]
  str x1, [sp, 8]
  str x2, [sp, 16]
  str x3, [sp, 24]
  str x4, [sp, 32]
  str x30, [sp, 40]


  mov x5, 1  // The return value
  mov x6, 0  // Current index within the string

  // Set these to 0 so we don't have other crap in there after ldrb
  mov x7, 0  // The value of the current byte in the string
  mov x8, 0  // The value of the current byte in the ring buffer

.Lringbuf_str_eq_loop:
  ldr x0, [sp, 32]
  cmp x6, x0
  b.ge .Lringbuf_str_eq_end
  
  // Load the string's byte
  ldr x0, [sp, 24]
  ldrb w7, [x0, x6]

  // Load the ring buffer's byte
  ldr x0, [sp, 16]
  add x0, x0, x6
  ldr x1, [sp, 8]
  bl modulo
  ldr x1, [sp, 0]
  ldrb w8, [x1, x0]

  add x6, x6, 1
  cmp x7, x8
  b.eq .Lringbuf_str_eq_loop

.Lringbuf_str_eq_false:
  mov x5, 0

.Lringbuf_str_eq_end:
  mov x0, x5
  ldr x30, [sp, 40]
  add sp, sp, 48
  ret
